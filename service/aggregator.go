package service

import (
    "sync"

    "gitlab.com/andriyr/seldon-engineering-coding-test/domain"
)

func SentenceAggregator(channels []<-chan *domain.Sentence) <-chan *domain.Sentence {
    var wg sync.WaitGroup
	  wg.Add(len(channels))

    aggregator := make(chan *domain.Sentence)

    for _, channel := range channels {
        go func(channel <-chan *domain.Sentence) {
            for sentence := range channel {
              aggregator <- sentence
            }

            wg.Done()
      }(channel)
    }

    go func() {
    		wg.Wait()
    		close(aggregator)
	  }()

    return aggregator
}
