package service

import (
    "io/ioutil"

		"gitlab.com/andriyr/seldon-engineering-coding-test/scanner"
    "gitlab.com/andriyr/seldon-engineering-coding-test/domain"
    "gitlab.com/andriyr/seldon-engineering-coding-test/statistics"
)


type Service struct {
    datadir string
}

func NewService(datadir string) *Service {
    return &Service{
        datadir: datadir,
    }
}

func (s *Service) ScanFile(filename string, filters []string) ([]*domain.WordCount, error) {
    sentences := scanner.ScanFile(s.datadir, filename, filters)
    words := scanner.ScanSentence(sentences, make([]string, 0))

    aggregateWordCount := statistics.CountWords(words)

    wordCounts := aggregateWordCount.HighestToLowest()

    return wordCounts, nil
}

func (s *Service) ScanFiles(filenames []string, filters []string) ([]*domain.WordCount, error) {
    var channels = []<-chan *domain.Sentence{}

    for _, filename := range filenames {
        sentences := scanner.ScanFile(s.datadir, filename, filters)
        words := scanner.ScanSentence(sentences, make([]string, 0))

        channels = append(channels, words)
    }

    aggregator := SentenceAggregator(channels)

    aggregateWordCount := statistics.CountWords(aggregator)

    wordCounts := aggregateWordCount.HighestToLowest()

    return wordCounts, nil
}

func (s *Service) ScanDirectory(filters []string) ([]*domain.WordCount, error) {
    files, _ := ioutil.ReadDir(s.datadir)

    var filenames []string
  	for _, file := range files {
        filenames = append(filenames, file.Name())
  	}

    return s.ScanFiles(filenames, filters)
}
