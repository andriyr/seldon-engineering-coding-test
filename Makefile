.PHONY: proto test lint build image clean

GOBIN = ./build/bin
GORUN = env GO111MODULE=on go run
VERSION = `cat version`

proto:
		$(GORUN) build/ci.go proto
		@echo "Done."

test:
		$(GORUN) build/ci.go test
		@echo "Done."

lint:
		${GOPATH}/bin/golangci-lint run
		@echo "Finished linting. Clean."

build:
		$(GORUN) build/ci.go build
		@echo "Finished build."

image:
		docker build . -t sect:$(VERSION) -f ./Dockerfile
		@echo "Finished image build."

clean:
		env GO111MODULE=on go clean -cache
		rm -rf $(GOBIN)/*
		rm -rf ./api/*pb.go
		@echo "Removed artifacts."

devtools:
		env GOBIN= go get -u github.com/golang/protobuf/protoc-gen-go
		curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b ${GOPATH}/bin v1.24.0
