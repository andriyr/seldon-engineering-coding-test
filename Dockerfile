# Build service inside separate container
FROM golang:1.14.1-alpine as builder

ADD . /seldon-engineering-coding-test

RUN apk add --no-cache make protobuf curl gcc musl-dev linux-headers git

RUN go get github.com/golang/protobuf/protoc-gen-go

# Install coding test
RUN cd /seldon-engineering-coding-test && make build


# Pull service into deployment container
FROM alpine:latest

COPY --from=builder /seldon-engineering-coding-test/build/bin/sect /usr/local/bin/

RUN apk add --no-cache ca-certificates

RUN mkdir /volume

EXPOSE 8081
ENTRYPOINT ["sect", "-datadir=/volume"]
