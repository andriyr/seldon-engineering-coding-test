package main

import (
    "context"
    "time"
    "log"
    "fmt"

    "gitlab.com/andriyr/seldon-engineering-coding-test/api"

    "google.golang.org/grpc"
)

var (
    GRPC_PORT = 8081
)


func directoryWordCounts(client api.StatisticsClient, req *api.DirectoryWordCountsRequest) {
  	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  	defer cancel()

  	res, err := client.DirectoryWordCounts(ctx, req)
  	if err != nil {
  		  log.Fatal(err)
  	}

    prettyPrint(res.WordCounts)
}

func fileWordCounts(client api.StatisticsClient, req *api.FileWordCountsRequest) {
  	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
  	defer cancel()

  	res, err := client.FileWordCounts(ctx, req)
  	if err != nil {
  		  log.Fatal(err)
  	}

    prettyPrint(res.WordCounts)
}

func prettyPrint(wordCounts []*api.WordCount) {
    for _, wordCount := range wordCounts {
        fmt.Printf("\nWord: %v, Count: %v \n", wordCount.Word, wordCount.Count)

        for _, occurrence := range wordCount.Occurrences {
            fmt.Printf("\t File: %v, Sentences: %v \n", occurrence.Source, occurrence.Positions)
        }
    }
}

func main() {
    var opts []grpc.DialOption
    opts = append(opts, grpc.WithInsecure())

    conn, err := grpc.Dial(fmt.Sprintf("localhost:%v", GRPC_PORT), opts...)
  	if err != nil {
  		  log.Fatal("Failed to connect: ", err)
  	}

    defer conn.Close()

    client := api.NewStatisticsClient(conn)

    //fileWordCounts(client, &api.FileWordCountsRequest{Filename: "doc1.txt"})

    directoryWordCounts(client, &api.DirectoryWordCountsRequest{})
}
