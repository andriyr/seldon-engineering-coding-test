package scanner

import (
    "bufio"
    "log"
    "os"
    "bytes"
    "strings"
    "path/filepath"

    "gitlab.com/andriyr/seldon-engineering-coding-test/domain"
)


/* Scan file and send through send sentences through channel */
func ScanFile(path, filename string, expressions []string) <-chan *domain.Sentence {
    sentences := make(chan *domain.Sentence, 10)

    go func(path, filename string, expressions []string) {
        file, err := os.Open(filepath.Join(path, filename))

        if err != nil {
            log.Fatal(err)
            return
        }

        defer file.Close()

        scanner := bufio.NewScanner(file)
        filter := NewFilter(expressions)
        scanner.Split(filter.Sentences)

        position := 0
        for {
            success := scanner.Scan()

            if !success {
                 err = scanner.Err()

                 if err == nil {
                     log.Println(filename + " EOF")
                 } else {
                     log.Fatal(err)
                 }

                 close(sentences)
                 break
             }

             sentences <- domain.NewSentence(filename, position, scanner.Bytes())

             position++
        }
    }(path, filename, expressions)

    return sentences
}

func ScanSentence(in <-chan *domain.Sentence, expressions []string) <-chan *domain.Sentence {
    sentences := make(chan *domain.Sentence, 10)

    go func(in <-chan *domain.Sentence, expressions []string) {
        for sentence := range in {
            scanner := bufio.NewScanner(bytes.NewReader(sentence.Bytes()))
            filter := NewFilter(expressions)
            scanner.Split(filter.Words)

            words := make([]string, 0)

            for {
                success := scanner.Scan()

                if !success {
                    err := scanner.Err()

                    if err != nil {
                       log.Fatal(err)
                    }

                    break
                }

                words = append(words, strings.ToLower(scanner.Text()))
            }

            sentence.SetWords(words)

            sentences <- sentence
      }

      close(sentences)
    }(in, expressions)

    return sentences
}
