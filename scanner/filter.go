package scanner

import (
    "fmt"
    "regexp"
    "unicode/utf8"
    "unicode"
)

/* This file contains extensions to the scanner package without being dependent
on it

See below for source file:
https://golang.org/src/bufio/scan.go */
type Filter struct {
    patterns []*regexp.Regexp
}

func NewFilter(expressions []string) *Filter {
    patterns := make([]*regexp.Regexp, 0)

    for _, expression := range expressions {
        fmt.Println(expression)
        pattern := regexp.MustCompile(expression)
        patterns = append(patterns, pattern)
    }

    return &Filter{
        patterns: patterns,
    }
}

func (f *Filter) Sentences(data []byte, atEOF bool) (advance int, token []byte, err error) {
  masked := f.mask(data)

  // Skip leading spaces.
	start := 0
  for width := 0; start < len(data); start += width {
		var r rune
		r, width = utf8.DecodeRune(data[start:])
		if !isSpace(r) {
			break
		}
	}

	// Scan until end of sentence, including last rune.
	for width, i := 0, start; i < len(data); i += width {
    if !masked[i] {
      var r rune
      r, width = utf8.DecodeRune(data[i:])
      if isSentenceEnd(r) {
        return i + width, data[start:i+width], nil
      }
    }
	}

	// If we're at EOF, we have a final, non-empty, non-terminated word. Return it.
	if atEOF && len(data) > start {
		return len(data), data[start:], nil
	}
	// Request more data.
	return start, nil, nil
}

func (f *Filter) Words(data []byte, atEOF bool) (advance int, token []byte, err error) {
    // Skip leading spaces.
    start := 0
    for width := 0; start < len(data); start += width {
      var r rune
      r, width = utf8.DecodeRune(data[start:])
      if unicode.IsLetter(r) {
        break
      }
    }
    // Scan until space, marking end of word.
    for width, i := 0, start; i < len(data); i += width {
      var r rune
      r, width = utf8.DecodeRune(data[i:])
      if !unicode.IsLetter(r) {
        return i + width, data[start:i], nil
      }
    }
    // If we're at EOF, we have a final, non-empty, non-terminated word. Return it.
    if atEOF && len(data) > start {
      return len(data), data[start:], nil
    }
    // Request more data.
    return start, nil, nil
}

/* Use regex to mask sections that would otherwise be picked up by the scan */
func (f *Filter) mask(slice []byte) []bool {
    mask := make([]bool, len(slice))

    for _, pattern := range f.patterns {
        locations := pattern.FindAllIndex(slice, -1)

        if locations != nil { //nolint:gosimple
            for _, location := range locations {
                mask = maskSection(mask, location)
            }
        }
    }

    return mask
}

func maskSection(mask []bool, location []int) []bool {
    length := location[1]-location[0]
    masked := make([]bool, length)

    for i := 0; i < length; i++ {
        masked[i] = true
    }

    return append(mask[:location[0]], append(masked, mask[location[1]:]...)...)
}

func isSpace(r rune) bool {
	if r <= '\u00FF' {
		// Obvious ASCII ones: \t through \r plus space. Plus two Latin-1 oddballs.
		switch r {
		case ' ', '\t', '\n', '\v', '\f', '\r':
			return true
		case '\u0085', '\u00A0':
			return true
		}
		return false
	}
	// High-valued ones.
	if '\u2000' <= r && r <= '\u200a' {
		return true
	}
	switch r {
	case '\u1680', '\u2028', '\u2029', '\u202f', '\u205f', '\u3000':
		return true
	}
	return false
}

func isSentenceEnd(r rune) bool {
    switch r {
    case '\u2024', '\u2025', '\u2026', '\ufe52', '\uff0e':
        return false
    case '\u002e': // '\n'
        return true
		}

		return false
}
