package server

import (
    "context"

    //"gitlab.com/andriyr/seldon-engineering-coding-test/domain"
    "gitlab.com/andriyr/seldon-engineering-coding-test/service"
    "gitlab.com/andriyr/seldon-engineering-coding-test/api"
    "gitlab.com/andriyr/seldon-engineering-coding-test/server/converter"
)

type StatisticsServer struct {
    service *service.Service
}

func NewStatisticsServer(service *service.Service) *StatisticsServer {
    return &StatisticsServer{
        service: service,
    }
}

func (s *StatisticsServer) FileWordCounts(ctx context.Context, req *api.FileWordCountsRequest) (*api.WordCountsResponse, error) {
  wordCounts, err := s.service.ScanFile(req.Filename, req.Filters)

  wordCountsResponse := converter.WordCountsToWordCountsResponse(wordCounts)

  return wordCountsResponse, err
}

func (s *StatisticsServer) DirectoryWordCounts(ctx context.Context, req *api.DirectoryWordCountsRequest) (*api.WordCountsResponse, error) {
  wordCounts, err := s.service.ScanDirectory(req.Filters)

  wordCountsResponse := converter.WordCountsToWordCountsResponse(wordCounts)

  return wordCountsResponse, err
}
