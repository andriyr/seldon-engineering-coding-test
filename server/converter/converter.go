package converter

import (
    "gitlab.com/andriyr/seldon-engineering-coding-test/domain"
    "gitlab.com/andriyr/seldon-engineering-coding-test/api"
)


func WordCountsToWordCountsResponse(wordCounts []*domain.WordCount) *api.WordCountsResponse {
    var response *api.WordCountsResponse

    wordCountsArr := make([]*api.WordCount, 0)
    for _, wordCount := range wordCounts {
        occurrences := make([]*api.Occurrence, 0)
        for _, occurrence := range wordCount.Occurrences() {
            occurrences = append(occurrences, &api.Occurrence{Source: occurrence.Source(), Positions: arrayIntToInt64(occurrence.Positions())})
        }

        wordCountsArr = append(wordCountsArr, &api.WordCount{Word: wordCount.Word(), Count: int64(wordCount.Count()), Occurrences: occurrences})
    }

    response = &api.WordCountsResponse{WordCounts: wordCountsArr}

    return response
}

func arrayIntToInt64(integers []int) []int64 {
    converted := make([]int64, 0)

    for _, integer := range integers {
        converted = append(converted, int64(integer))
    }

    return converted
}
