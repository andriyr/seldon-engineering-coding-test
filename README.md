
# Coding Test: Important Hashtags

Using the documents in the `test-docs` folder, find the most commonly occuring words (or "Most Important Hashtags").

Your solution should include a programmatic interface (CLI, API, Seldon Deployment, etc) that creates the following output sorted by the most important words:

```
+-------+-----------------+------------+
| Word  |   Sentences     | Documents  |
+-------+-----------------+------------+
| w1    | s1, s2, s3, s4  | d1, d2     |
| w2    | s3, s4, s5      | d2, d3, d4 |
| …     | …               | …          |
+-------+-----------------+------------+
```

This is an example, you have the flexibility to modify the final output as you wish.

The idea is to build a reusable solution which can be extended to other documents and text sources.

Please feel free to include a README.md file with any relevant information related to your solution.

# Pre-requisites

To build the stand-alone binary you will require:

 - go 1.14
 - protoc https://developers.google.com/protocol-buffers/
 - make
 - curl

# Installation

Please ensure you have the pre-requisites listed above.

Check out the repository and download devtools:

```
git clone https://gitlab.com/andriyr/seldon-engineering-coding-test.git && \
cd ./seldon-engineering-coding-test && \
make devtools
```

Ensure the PATH environment variable contains go bin. You can append this line
at the end of $HOME/.profile for persistence.

```
export PATH=$PATH:$HOME/go/bin
```

## Building the stand-alone service

To build to the local directory (./build/bin) run the command below:

```
make build
```

To build to a specific directory run:

```
make build GOBIN=/your/target/dir
```

## Building a docker image

To create a docker image run the command below. This will use the default docker
installation on your system and is not included as part of devtools.

```
make image
```

# Starting the service

You can start the service as a daemon or container.

## Running as a stand-alone service

From the repository root folder run the following command:

```
chmod 755 ./build/bin/sect && \
./build/bin/sect -datadir=./test-docs
```

In a second terminal window run the following script to test the api:

```
go run ./examples/count_example.go
```

## Running as a container

Once the image is built start the container with the following command:

```
docker run -p 8081:8081 -v $(pwd)/test-docs:/volume --name sect sect:0.0.1
```

In a second terminal window run the following script to test the api:

```
go run ./examples/count_example.go
```

To remove the container run:

```
docker rm sect
```
