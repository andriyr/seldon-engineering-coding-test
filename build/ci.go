/*
Usage: go run build/ci.go <command> <command flags/arguments>

Available commands:

   proto -- generates proto files and services
   test  -- runs all tests
   lint  -- runs linters
   build -- builds service binary

*/
package main

import (
    "os"
    "os/exec"
    "log"
    "path/filepath"
)


var (
    WORKDIR, _ = os.Getwd()
    GOBIN = filepath.Join(WORKDIR, "build", "bin")
)

func main() {
    if _, err := os.Stat(filepath.Join("build", "ci.go")); os.IsNotExist(err) {
  		log.Fatal("This script must be run from the root of the repository.")
  	}

  	if len(os.Args) < 2 {
  		log.Fatal("Missing command.")
  	}

  	switch os.Args[1] {
  	case "proto":
  		  proto()
  	case "test":
  		  test()
  	case "build":
  		  build()
  	default:
  		  log.Fatal("Unknown command: ", os.Args[1])
  	}
}

func proto() {
    cmd := exec.Command("protoc", "-I", "api/", "api/statistics.proto", "--go_out=plugins=grpc:api")
    cmd.Env = os.Environ()
    cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout

    if err := cmd.Run(); err != nil {
        log.Fatal("Unable to build proto files: ", err)
    }
}

func test() {
    proto()

    cmd := exec.Command("go", "test", "-v", "./..." )
    cmd.Env = os.Environ()
    cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout

    if err := cmd.Run(); err != nil {
        log.Fatal("Couldn't run tests: ", err)
    }
}

func build() {
    proto()

    cmd := exec.Command("go", "build", "-o", filepath.Join(GOBIN, "sect"), filepath.Join(WORKDIR, "main.go"))
    cmd.Env = os.Environ()
    cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout

    if err := cmd.Run(); err != nil {
        log.Fatal("Failed to compile: ", err)
    }
}
