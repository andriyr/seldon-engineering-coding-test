package domain


type Sentence struct {
    source   string
    position int
    sequence []byte
    words []string
}

func NewSentence(source string, position int, sequence []byte) *Sentence {
    return &Sentence{
        source: source,
        position: position,
        sequence: sequence,
        words: nil,
    }
}

func (s *Sentence) Source() string {
    return s.source
}

func (s *Sentence) Position() int {
    return s.position
}

func (s *Sentence) Bytes() []byte {
    return s.sequence
}

func (s *Sentence) Text() string {
    return string(s.sequence)
}

func (s *Sentence) SetWords(words []string) {
    s.words = words
}

func (s *Sentence) Words() []string {
    return s.words
}
