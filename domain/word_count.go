/*
A doubly linked list which contains unique word counts, with the lowest valued
at Front and highest at Back. A Hashmap to each unique word allows O(1) inserts,
a hashmap to Back of each count section allows O(1) count increments.

The hashmap to count back is used to skip the long tails of a normal distribution
of words in a body of text.

Order highest to lowest: O(n)
Order lowest to highest: O(n)
Insert: O(1)
Update: O(1)
 */
package domain

import (
    "container/list"
)


type AggregateWordCount struct {
    words map[string]*list.Element
    counts map[int]*list.Element
    list *list.List
}

func NewAggregateWordCount() *AggregateWordCount {
    return &AggregateWordCount{
        words: make(map[string]*list.Element),
        counts: make(map[int]*list.Element),
        list: list.New(),
    }
}

func (a *AggregateWordCount) WordCount(word string) *WordCount {
    return a.words[word].Value.(*WordCount)
}

func (a *AggregateWordCount) Highest() *WordCount {
    front := a.list.Back()

    if front != nil {
        return front.Value.(*WordCount)
    }

    return nil
}

func (a *AggregateWordCount) Lowest() *WordCount {
    back := a.list.Front()

    if back != nil {
        return back.Value.(*WordCount)
    }

    return nil
}

/* If the word hasn't been indexed push to front and register with maps. Otherwise
get back of current section and move the element after the back as we are only
incrementing by one at a time. Update maps and check if previous section map needs
to be updated. */
func (a *AggregateWordCount) AddOccurrence(word, source string, position int) {
    var current *list.Element

    current = a.words[word]

    if current == nil {
        current = a.list.PushFront(NewWordCount(word, source, position))

        a.words[word] = current

        if sectionBack := a.counts[1]; sectionBack == nil {
            a.counts[1] = current
        }
    } else {
        currentCount := current.Value.(*WordCount).Count()
        sectionBack := a.counts[currentCount]

        current.Value.(*WordCount).AddOccurrence(source, position)

        if !current.Value.(*WordCount).Equals(sectionBack.Value.(*WordCount)) {
            a.list.MoveAfter(current, sectionBack)
        } else {
            previous := current.Prev()

            if previous == nil {
                delete(a.counts, currentCount)
            } else {
                if previous.Value.(*WordCount).Count() == currentCount {
                    a.counts[currentCount] = previous
                } else {
                    delete(a.counts, currentCount)
                }

                a.list.MoveAfter(current, previous)
            }
        }

        if newSectionBack := a.counts[currentCount+1]; newSectionBack == nil {
            a.counts[currentCount+1] = current
        }
    }
}

/* Get back and move to front */
func (a *AggregateWordCount) HighestToLowest() []*WordCount {
    wordCounts := make([]*WordCount, 0)

    element := a.list.Back()
    for element != nil {
        wordCounts = append(wordCounts, element.Value.(*WordCount))

        element = element.Prev()
    }

    return wordCounts
}

/* Get front and back to back. */
func (a *AggregateWordCount) LowestToHighest() []*WordCount {
    wordCounts := make([]*WordCount, 0)

    element := a.list.Front()
    for element != nil {
        wordCounts = append(wordCounts, element.Value.(*WordCount))

        element = element.Next()
    }

    return wordCounts
}


type WordCount struct {
    word string
    count int
    occurrences map[string]*Occurrence
    keys []string
}

func NewWordCount(word, source string, position int) *WordCount {
    wordCount := &WordCount{
        word: word,
        count: 0,
        occurrences: make(map[string]*Occurrence),
        keys: make([]string, 0),
    }

    wordCount.AddOccurrence(source, position)

    return wordCount
}

func (w *WordCount) Word() string {
    return w.word
}

func (w *WordCount) Count() int {
    return w.count
}

func (w *WordCount) Occurrences() []*Occurrence {
    occurrences := make([]*Occurrence, 0)

    for _, key := range w.keys {
        occurrences = append(occurrences, w.occurrences[key])
    }

    return occurrences
}

func (w *WordCount) AddOccurrence(source string, position int) {
    w.count += 1

    occurrence := w.occurrences[source]

    if occurrence == nil {
        w.occurrences[source] = NewOccurrence(source, position)
        w.keys = append(w.keys, source)
        return
    }

    occurrence.AddPosition(position)
}

func (w *WordCount) Equals(other *WordCount) bool {
    return w.word == other.Word()
}

type Occurrence struct {
    source string
    positions map[int]bool
    keys []int
}

func NewOccurrence(source string, position int) *Occurrence {
    occurrence := &Occurrence{
        source: source,
        positions: make(map[int]bool),
        keys: make([]int, 0),
    }

    occurrence.AddPosition(position)

    return occurrence
}

func (o *Occurrence) Source() string {
    return o.source
}

func (o *Occurrence) Positions() []int {
    positions := make([]int, 0)

    for _, key := range o.keys {
        positions = append(positions, key)
    }

    return positions
}

func (o *Occurrence) AddPosition(p int) {
    position := o.positions[p]

    if !position {
        o.positions[p] = true
        o.keys = append(o.keys, p)
    }
}
