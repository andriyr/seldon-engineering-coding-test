package domain

import (
    "reflect"
    "testing"
)


// Ensure pointer to highest word count is updated
func TestAggregateWordCountHighest(t *testing.T) {
    aggregate := NewAggregateWordCount()

    tables := []struct {
    		word string
    		source string
    		position int
        highest string
  	}{
    		{"word", "source", 1, "word"},
    		{"word", "source", 1, "word"},
    		{"word1", "source", 1, "word"},
    		{"word1", "source", 1, "word"},
        {"word1", "source", 1, "word1"},
  	}

  	for _, table := range tables {
        aggregate.AddOccurrence(table.word, table.source, table.position)

        highest := aggregate.Highest().Word()
    		if highest != table.highest {
    			   t.Errorf("Highest word was incorrect, got: %v, want: %v.", highest, table.highest)
    		}
  	}
}

// Ensure pointer to lowest count is updated
func TestAggregateWordCountLowest(t *testing.T) {
    aggregate := NewAggregateWordCount()

    tables := []struct {
    		word string
    		source string
    		position int
        lowest string
  	}{
    		{"word", "source", 1, "word"},
    		{"word1", "source", 1, "word1"},
    		{"word1", "source", 1, "word"},
    		{"word", "source", 1, "word"},
        {"word", "source", 1, "word1"},
  	}

  	for _, table := range tables {
        aggregate.AddOccurrence(table.word, table.source, table.position)

        lowest := aggregate.Lowest().Word()
    		if lowest != table.lowest {
    			   t.Errorf("Lowest word was incorrect, got: %v, want: %v.", lowest, table.lowest)
    		}
  	}
}

// Ensure count for each word is updated correctly
func TestAggregateWordCountAddOccurrence(t *testing.T) {
    aggregate := NewAggregateWordCount()

    tables := []struct {
    		word string
    		source string
    		position int
        count int
  	}{
    		{"word", "source", 1, 1},
    		{"word", "source", 1, 2},
    		{"word1", "source", 1, 2},
        {"word", "source", 1, 3},
  	}

  	for _, table := range tables {
        aggregate.AddOccurrence(table.word, table.source, table.position)

        count := aggregate.WordCount("word").Count()
    		if count != table.count {
    			   t.Errorf("Count was incorrect, got: %v, want: %v.", count, table.count)
    		}
  	}
}

/* Ensure count and occurences are added to WordCount correctly */
func TestWordCountAddOccurrence(t *testing.T) {
    wordCount := NewWordCount("word", "source", 1)

    tables := []struct {
    		source string
    		position int
        count int
        sources []string
  	}{
    		{"source", 1, 2, []string{"source"}},
    		{"source1", 1, 3, []string{"source", "source1"}},
    		{"source2", 1, 4, []string{"source", "source1", "source2"}},
        {"source3", 1, 5, []string{"source", "source1", "source2", "source3"}},
  	}

  	for _, table := range tables {
        wordCount.AddOccurrence(table.source, table.position)

        count := wordCount.Count()
        sources := make([]string, 0)
        for _, occurrence := range wordCount.Occurrences() {
            sources = append(sources, occurrence.Source())
        }

    		if count != table.count {
    			   t.Errorf("Count was incorrect, got: %v, want: %v.", count, table.count)
    		}

        if !reflect.DeepEqual(sources, table.sources) {
            t.Errorf("Sources were incorrect, got: %v, want: %v.", sources, table.sources)
        }
  	}
}

func TestWordCountEquals(t *testing.T) {
    wordCount := NewWordCount("word", "source", 1)

    tables := []struct {
    		word string
    		source string
        position int
        equals bool
  	}{
    		{"word", "source1", 1, true},
    		{"source1", "source", 1, false},
    		{"source2", "source", 1, false},
        {"source3", "source", 1, false},
  	}

  	for _, table := range tables {
        otherWordCount := NewWordCount(table.word, table.source, table.position)

        equals := wordCount.Equals(otherWordCount)

    		if equals != table.equals {
    			   t.Errorf("Equals was incorrect, got: %v, want: %v.", equals, table.equals)
    		}
  	}
}

func TestOccurrenceAddPosition(t *testing.T) {
    occurrence := NewOccurrence("source", 0)

    tables := []struct {
    		source string
        position int
        positions []int
  	}{
    		{"source", 0, []int{0}},
    		{"source", 0, []int{0}},
    		{"source", 1, []int{0, 1}},
        {"source", 2, []int{0, 1, 2}},
  	}

    for _, table := range tables {
        occurrence.AddPosition(table.position)
        positions := occurrence.Positions()

        if !reflect.DeepEqual(positions, table.positions) {
            t.Errorf("Positions were incorrect, got: %v, want: %v.", positions, table.positions)
        }
  	}
}
