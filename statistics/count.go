package statistics

import (
		"gitlab.com/andriyr/seldon-engineering-coding-test/domain"
)

func CountWords(in <-chan *domain.Sentence) *domain.AggregateWordCount {
		count := domain.NewAggregateWordCount()

		for sentence := range in {
				words := sentence.Words()

				for _, word := range words {
						count.AddOccurrence(word, sentence.Source(), sentence.Position())
				}
		}

		return count
}
