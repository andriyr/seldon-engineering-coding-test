/*
Available Flags:

  -port    -- Sets the grpc port, 8081 by default, example: -port=8082
  -datadir -- Sets the directory from where to scan files, defaults to $HOME, example: -datadir=/home/andriyr/Documents

*/
package main

import (
    "os"
    "log"
    "flag"
    "fmt"
    "net"
    "path/filepath"

    "gitlab.com/andriyr/seldon-engineering-coding-test/service"
    "gitlab.com/andriyr/seldon-engineering-coding-test/api"
    "gitlab.com/andriyr/seldon-engineering-coding-test/server"

    "google.golang.org/grpc"
)

func main() {
    home, err := os.UserHomeDir()

    if err != nil {
  		log.Fatal("Couldn't stat current user.")
  	}

    portFlag := flag.Int("port", 8081, "gRPC port number.")
    dataDirFlag := flag.String("datadir", home, "Target directory for reading files")
    flag.Parse()

    address := fmt.Sprintf(":%d", *portFlag)
    dataDir, _ := filepath.Abs(*dataDirFlag)

    log.Printf("Starting server with address localhost%v and datadir: %v", address, dataDir)

    // Start Service
    service := service.NewService(dataDir)

    // Start gRPC server
    listener, err := net.Listen("tcp", address)
    if err != nil {
        log.Fatal("Failed to start listener: ", err)
    }
    grpcServer := grpc.NewServer()
    api.RegisterStatisticsServer(grpcServer, server.NewStatisticsServer(service))

    err = grpcServer.Serve(listener)

    if err != nil {
        log.Fatal("Failed to start gRPC server: ", err)
    }
}
